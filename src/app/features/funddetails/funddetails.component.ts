import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-funddetails',
  templateUrl: './funddetails.component.html',
  styleUrls: ['./funddetails.component.scss']
})
export class FunddetailsComponent implements OnInit {
  fundname;
  paramValue;
  constructor(private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.paramValue = this.route.params.subscribe(params => {
      this.fundname = params['fundname'];
      });
      console.log(this.fundname);

  }

  navigateCompare()
  {
    //alert('navigate to compate');
    this.router.navigate(['/Comparefunds', this.fundname]);
  }

}
