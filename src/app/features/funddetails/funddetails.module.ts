import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MaterialModule } from 'src/app/core/material.module';
import { FunddetailsComponent } from './funddetails.component';

@NgModule({
  imports: [CommonModule,FormsModule,
    ReactiveFormsModule,MaterialModule],
   exports: [],
   declarations: [FunddetailsComponent],
   entryComponents: [FunddetailsComponent]
})
export class FunddetailsModule {
}
