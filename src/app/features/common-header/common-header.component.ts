import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ActivatedRoute ,Router} from '@angular/router';
import { FundsService } from '../../shared/service/funds.service';

@Component({
  selector: 'app-common-header',
  templateUrl: './common-header.component.html',
  styleUrls: ['./common-header.component.scss']
})
export class CommonHeaderComponent implements OnInit {
  fundListJson: string[];
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  constructor(
    private router:Router,  private fundsService: FundsService
  ) { }
  ngOnInit() {
    this.getFunds();  
  }
  getFunds(): void {
    this.fundsService.getFunds()
    .subscribe((data) => {
      this.fundListJson = data.map(elem => elem.name);
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
        );
      
      });
    }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.fundListJson.filter(option => option.toLowerCase().includes(filterValue));
  }
  
}
