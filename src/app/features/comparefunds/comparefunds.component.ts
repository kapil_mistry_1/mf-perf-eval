import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AutoCompleteModalComponent } from '../autocomplete/autocompleteModal.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comparefunds',
  templateUrl: './comparefunds.component.html',
  styleUrls: ['./comparefunds.component.scss']
})
export class ComparefundsComponent implements OnInit {
  paramValue;
  fundname;
  dialogValue


  constructor(public matDialog: MatDialog, private route:ActivatedRoute) { }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    const modalDialog = this.matDialog.open(AutoCompleteModalComponent, dialogConfig);
    /*modalDialog.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
      this.dialogValue = result.data;
    });*/
  }
  

  ngOnInit(): void {
    this.paramValue = this.route.params.subscribe(params => {
      this.fundname = params.fundname;;

      });
      
  }
  switchToAddFund() {
    this.fundname = '';
  }

}
