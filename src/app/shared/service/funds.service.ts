import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Funds } from './funds.model';

@Injectable({
  providedIn: 'root'
})
export class FundsService {
  private fundsUrl = 'assets/data/';  // URL to json file
  constructor(private http: HttpClient) { }

  /** GET all funds */
  getFunds (): Observable<Funds[]> {
    return this.http.get<Funds[]>(this.fundsUrl + 'funds.json');
  }

  /** GET specific funds */
  getFundsById (id): Observable<Funds[]> {
    return this.http.get<Funds[]>(this.fundsUrl + 'funds.json')
      .pipe(
          map(data => data.filter(element => element.id === id))
      )
  }

}
